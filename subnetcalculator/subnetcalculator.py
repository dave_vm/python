import sys
#import sys(Als een van de ingevoerde waarden niet geldig is, wordt het script stopgezet na de controle. Gebruik hiervoor sys.exit(0).)
#functie aanvragen ip adres en subnetmask
def ask_for_number_sequence(message):
  numberlist = input(message)
  return [int(elem) for elem in numberlist.split(".")]

def is_valid_ip_adres(numberlist):
    if len(numberlist) == 4:
        for elem in numberlist:
            if elem >= 0 and elem <= 255 :
                return
            else:
                return False
    else:
        return False
        
def binary_netmask(numberlist):
      bin_list= ""
      for elem in numberlist:
        bin_list += str(f"{elem:08b}")
      return bin_list
  
def one_bits_in_netmask(numberlist):
      counter = 0
      bin_num = binary_netmask(numberlist)
      for elem in bin_num:
          if elem == "1":
              counter += 1
      return counter

def prefix_length_to_max_hosts(prefix):
      hosts = (2**(32 - prefix)-2)
      return hosts
    
def apply_network_mask(host_address,netmask):
    network_address = []
    for x in range(4):
       network_address.append(host_address[x] & netmask[x])
    return network_address

def is_valid_netmask(numberlist):
    checking_ones = True
    binary_net = binary_netmask(numberlist)
    if len(numberlist) == 4:
        for elem in binary_net:
            if elem == "0":
                checking_ones = False
            elif elem == "1" and checking_ones == False:
                  return False
        return True
    else:
        return False

def netmask_to_wilcard_mask(netmask):
  wildcard = []
  for elem in netmask:
      reverse_elem = ""
      elem_string = str(f"{elem:08b}")
      for subelem in elem_string:
          if subelem == "0":
              reverse_elem += "1"
          if subelem == "1":
              reverse_elem += "0"
      wildcard.append(int(reverse_elem,2))
  return wildcard

#bitwise OR gebruiken hier(opzoeken wiki.python.org, journal.dev.com) poging 27
def get_broadcast_address(network_address,wildcard_mask):
      broadcast_address = []
      for x in range(4):
          broadcast_address.append(network_address[x] | wildcard_mask[x])
      return broadcast_address
  
IpAdres = ask_for_number_sequence("Wat is het Ipadres? \n")
Subnet = ask_for_number_sequence("Wat is het subnetmasker? \n")

if is_valid_ip_adres(IpAdres) == False or is_valid_netmask(Subnet) == False:
    print("Dit is geen geldig ipadres en/of subnetmasker!")
    sys.exit(0)
else:
    print("IP-adres en subnetmasker zijn geldig.")
    prefix = one_bits_in_netmask(Subnet)
    print("De lengte van het subnetmasker is",prefix,".")
    binair = binary_netmask(Subnet)
    print( "Het binairmasker is",binair,".")
    network_addr = apply_network_mask(IpAdres,Subnet)
    network_addr2 = ".".join(str(elem) for elem in network_addr)
    print("Het adres van het subnet is",network_addr2,".")
    wildcard = netmask_to_wilcard_mask(Subnet)
    wildcard2 = ".".join(str(elem) for elem in wildcard)
    print( "Het wildcardmasker is",wildcard2,".")
    broadcast = get_broadcast_address(IpAdres,wildcard)
    broadcast2 = ".".join(str(elem) for elem in broadcast)
    print("Het broadcast adres is",broadcast2,".")
    max_hosts = prefix_length_to_max_hosts(prefix)
    print("Het maximum aantal hosts op dit subnet is",max_hosts,".")
    





  
  
