import subprocess
with open("hosts_and_ports.txt") as hp_fh:
    hp_content = hp_fh.readlines()
    for hp_pair in hp_content:
        iplist = hp_pair.split(":")
    
        with open("commands.txt") as fh:
                completed = subprocess.run(f"ssh dave@{iplist[0]} -p {iplist[1]}", capture_output=True, text=True, shell=True, stdin=fh)
